# Feedback-O-Matic

Feedback-O-Matic is a serverless web application where employees can input all the feedback they have for others while it is still fresh in their minds. The feedback can be viewed later when someone needs to fill in a peer review, and can even be filtered down.

## Getting Started


### Prerequisites

* Set up [AWS Cognito with Google SSO](https://docs.aws.amazon.com/cognito/latest/developerguide/getting-started.html) and insert the Identity Pool ID in *js/dynamo.js*

```
AWS.CognitoIdentityCredentials({
    IdentityPoolId: *insert here*,
    Logins: {
      'accounts.google.com': id_token
    }
  });
```
* [Create a table in AWS DynamoDB](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.html) called feedback-table with the following keys:

```
email, timestamp, author, feedback, tags
```


## Deployment

[Create a S3 bucket for static hosting](https://docs.aws.amazon.com/AmazonS3/latest/user-guide/static-website-hosting.html), upload the code and you're good to go!


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* [Sean Bramhall](https://github.com/seanbramhall) - *Initial work* 
* [Carlos Martell](https://github.com/cr-cmartell) - *Initial work* 
* [Teresa To](https://github.com/teresato) - *Initial work* 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
