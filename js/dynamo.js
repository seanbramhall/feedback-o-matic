var feedback = [];

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  author_fullName = profile.getName();
  author_email = profile.getEmail();

  var id_token = googleUser.getAuthResponse().id_token;
  console.log("ID Token: " + id_token);

  AWS.config.region = 'us-east-1'; // Region
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: '*insert here*',
    Logins: {
      'accounts.google.com': id_token
    }
  });

  dynamodb = new AWS.DynamoDB();
  docClient = new AWS.DynamoDB.DocumentClient();

  document.getElementById('welcome').innerHTML = 'Welcome, ' + author_fullName + "!";
  document.getElementById('author_name').innerHTML = author_fullName;
};

function validateEmail(email) {
  if (email.match(/(\w)+\.(\w)+@cloudreach.com/)) {
    return true;
  } else {
    alert("Naw mate, email must be in the first.last@cloudreach.com format.");
    return false;
  }
}

function createItem() {
  var feedback_email = document.getElementById('feedback_email').value
  var feedback_content = document.getElementById('feedback_content').value
  var feedback_tags = document.getElementById('feedback_tags').value
  var params = {
    TableName: "feedback-table",
    Item: {
      "email": feedback_email,
      "timestamp": Date.now(),
      "author": author_email,
      "feedback": feedback_content,
      "tags": feedback_tags.split(",").map(function(item) {
        return item.trim();
      })
    }
  };
  if (validateEmail(feedback_email)) {
    docClient.put(params, function(err, data) {
      if (err) {
        document.getElementById('textarea').innerHTML = "Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2);
      } else {
        document.getElementById('modal_content').innerHTML = "Feedback submitted successfully.";
        $('#successModal').modal('show');
        document.forms["feedback_form"].reset();
      }
    });
  }
}

function queryByEmail() {
  for (var i = feedback.length; i > 0; i--) {
    feedback.pop();
  }

  var feedback_email = document.getElementById('feedback_email').value

  var params = {
    TableName: "feedback-table",
    KeyConditionExpression: "email = :email",
    ExpressionAttributeValues: {
      ":email": feedback_email
    }
  };

  if (validateEmail(feedback_email)) {
    document.getElementById('feedback_textarea').innerHTML = "";
    document.getElementById('feedback_textarea').innerHTML += '<p style="margin-top:10px">Feedback left for ' + feedback_email + ":\n\n</p>";
    docClient.query(params, function(err, data) {
      if (err) {
        document.getElementById('feedback_textarea').innerHTML += "Please enter a valid email address.";
      } else {
        data.Items.forEach(function(item) {
          document.getElementById('feedback_textarea').innerHTML += '<p class="mb-0">' + item.feedback + '</p>\n<footer class="blockquote-footer"> Tagged with: ' + item.tags + '</footer>';
          feedback.push({
            'feedback': item.feedback,
            'tags': item.tags,
            'time': item.timestamp
          });
        });
      }
    });
  }
}

function filterByTags() {
  //Get tags provided by user to filter on
  var tags = document.getElementById('feedback_tags').value;
  var tags_arr = tags.split(',');

  //Clear the text area
  document.getElementById('feedback_textarea').innerHTML = '';

  //Loop through user's filter tags
  tags_arr.forEach(function(tag) {
    //Loop through the feedback returned from the search query
    feedback.forEach(function(item) {
      //If filter tag is in feedback tags
      if (item.tags.indexOf(tag) >= 0) {
        //Add the feedback to the textarea
        document.getElementById('feedback_textarea').innerHTML += '<p class="mb-0">' + item.feedback + '</p>\n<footer class="blockquote-footer"> Tagged with: ' + item.tags;
      }
    });
  })
}

function queryByEmailAndTime() {
  var email = document.getElementById('email_input').value
  var timestamp = document.getElementById('timestamp_input').value
  document.getElementById('textarea').innerHTML = "";
  document.getElementById('textarea').innerHTML += "Querying for feedback for " + email;

  var params = {
    TableName: "feedback-table",
    KeyConditionExpression: "email = :email AND #timest = :timest",
    ExpressionAttributeValues: {
      ":email": email,
      ":timest": parseInt(timestamp)
    },
    ExpressionAttributeNames: {
      "#timest": "timestamp"
    }
  };

  docClient.query(params, function(err, data) {
    if (err) {
      document.getElementById('textarea').innerHTML += "Unable to query. Error: " + "\n" + JSON.stringify(err, undefined, 2);
    } else {
      data.Items.forEach(function(item) {
        document.getElementById('textarea').innerHTML += "\n" + "\nFeedback: " + item.feedback + "\nAuthor: " + item.author + "\nSubmitted on: " + new Date(item.timestamp).toDateString();
      });

    }
  });
}
